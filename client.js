
const { SumRequest, SumResponse } = require("./calculator_pb")
const { CalculatorClient } = require("./calculator_grpc_web_pb")
var client = new CalculatorClient('http://localhost:8080');

Object.prototype.sendSumRequestToCPPService = function(){

    var request = new SumRequest()
    request.setA($("#valueA").val());
    request.setB($("#valueB").val());
		
	client.add(request, {}, (err, response) => {
		console.log("Result of Add : ");
		console.log(response.getResult());		
        $("#sumResult").text(response.getResult());
	});
}
