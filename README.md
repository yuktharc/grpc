Steps to run the application:

Step 1: 

	$ protoc -I . --cpp_out=. calculator.proto

Step 2: Run make command

	$ make
	
Step 3: Run the calculator grpc service

	$ ./calculator_server

Step 4: The below command will generate calculator_pb.js (which has message definitions) and calculator_grpc_web_pb.js (which has our client implementation).

	protoc calculator.proto --js_out=import_style=commonjs,binary:. --grpc-web_out=import_style=commonjs,mode=grpcwebtext:.


Step 5: Package our JavaScript code into a minified file.

		$ npm install
	
	The above command installs the dependent modules specified in package.json
	
		$ npx webpack client.js
	
	The above cammand packages client.js into dist/main.js with all the dependencies.
	NPM - Manages packages but doesn't make life easy executing any.
	NPX - A tool for executing Node packages.

Step 6:

	$ python3 -m http.server 8081 &

Step 7:

	$ [sudo] docker build -t my-envoy:1.0 .

Step 8:
	
	$ [sudo] docker run -d -p 8080:8080 -p 9901:9901 --network=host my-envoy:1.0

Step 9: Load the browser at http://localhost:8081